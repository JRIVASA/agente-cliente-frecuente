Attribute VB_Name = "Mod_CliFre"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long
    
Type Estruc_Config
    ArchivoInicio                                       As String
    Servidor                                            As String
    BaseDatos                                           As String
    Usuario                                             As String
    Password                                            As String
    ServidorAdm                                         As String
    BaseDatosAdm                                        As String
    UsuarioAdm                                          As String
    PasswordAdm                                         As String
    Localidad                                           As String
    LocalidadOrigen                                     As String
    Deposito                                            As String
    Ruta_Archivo                                        As String
    Separador                                           As String
    MontoPunto                                          As Double
    RutaBatch                                           As String
    Interfaz                                            As Boolean
    NoFrecuentes                                        As Boolean
    ImprimirArchivo                                     As Boolean
    ActualizarSucursalesOrigen                          As Boolean
End Type

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Public mInterfaz                                        As New cls_CliFre
Public ConexOrigen                                      As New ADODB.Connection
Global Configuracion                                    As Estruc_Config
Global Const IdProducto                                 As Long = 859

Global Const gCodProducto = 859
Global Const gNombreProducto = "Stellar BUSINESS"
Global gPK As String

Private mClsTmp As Object

Public Function sGetIni(ByVal sIniFile As String, ByVal sSection As String, _
ByVal sKey As String, ByVal sDefault As String) As String
    
    Const ParamMaxLength = 10000
    
    Dim sTemp As String * ParamMaxLength
    Dim nLength As Integer
    
    sTemp = Space$(ParamMaxLength)
    
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, ParamMaxLength, sIniFile)
    
    sGetIni = Left$(sTemp, nLength)
    
End Function

Public Sub sWriteIni(ByVal sIniFile As String, ByVal sSection As String, _
ByVal sKey As String, ByVal sData As String)
    WritePrivateProfileString sSection, sKey, sData, sIniFile
End Sub

Sub Main()
    
    gPK = Chr(83) & Chr(81) & Chr(76) & "_" _
    & Chr(51) & Chr(55) & Chr(73) & Chr(51) _
    & Chr(88) & Chr(50) & Chr(49) & Chr(83) _
    & Chr(78) & Chr(68) & Chr(65) & Chr(74) _
    & Chr(68) & Chr(75) & Chr(87) & "-" _
    & Chr(57) & Chr(48) & Chr(72) & Chr(71) _
    & Chr(52) & Chr(50) & Chr(48) & "_" & Chr(90)
    
    If App.PrevInstance Then
        End
    End If
    
    On Error GoTo Err_Descripcion
    
    Dim NewUser As String, NewPassword As String
    
    If IniciarProceso Then
        
        If CrearConexion(ConexOrigen, Configuracion.UsuarioAdm, _
        Configuracion.BaseDatosAdm, Configuracion.ServidorAdm, _
        Configuracion.PasswordAdm, True, NewUser, NewPassword, True) Then
            
            If NewUser <> "" Or NewPassword <> "" Then
                sWriteIni Configuracion.ArchivoInicio, "CONFIG_ADM", "USER_ADM", NewUser
                sWriteIni Configuracion.ArchivoInicio, "CONFIG_ADM", "PWD_ADM", NewPassword
            End If
            
            mInterfaz.ActualizarClientesFrecuentes ConexOrigen
            
        End If
        
    End If
    
    End
    
    Exit Sub
    
Err_Descripcion:
    
    'MsgBox Err.Description
    
End Sub

Private Function IniciarProceso() As Boolean
    
    Dim mValorTemp As Double
    
    With Configuracion
        
        .ArchivoInicio = App.Path & "\Configuracion.ini"
        .Localidad = sGetIni(.ArchivoInicio, "CONFIGURACION", "Localidad", "")
        .LocalidadOrigen = sGetIni(.ArchivoInicio, "CONFIGURACION", "LocalidadOrigen", "")
        
        '.Separador = sGetIni(.ArchivoInicio, "CONFIGURACION", "SEPARADOR", "|")
        '.Ruta_Archivo = sGetIni(.ArchivoInicio, "CONFIGURACION", "RUTA_ARCHIVOS", App.Path)
        mValorTemp = sGetIni(.ArchivoInicio, "CONFIGURACION", "VALORPUNTO", 0)
        '.RutaBatch = sGetIni(.ArchivoInicio, "CONFIGURACION", "RUTABATCH", "")
        .MontoPunto = IIf(IsNumeric(mValorTemp), mValorTemp, 0)
        
        .ServidorAdm = sGetIni(.ArchivoInicio, "CONFIG_ADM", "SRV_ADM", "")
        .BaseDatosAdm = sGetIni(.ArchivoInicio, "CONFIG_ADM", "BD_ADM", "VAD10")
        .UsuarioAdm = sGetIni(.ArchivoInicio, "CONFIG_ADM", "USER_ADM", "SA")
        .PasswordAdm = sGetIni(.ArchivoInicio, "CONFIG_ADM", "PWD_ADM", "")
        
        If Not (UCase(.UsuarioAdm) = "SA" And Len(.PasswordAdm) = 0) Then
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If Not mClsTmp Is Nothing Then
                .UsuarioAdm = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .UsuarioAdm)
                .PasswordAdm = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .PasswordAdm)
            End If
            
        End If
        
        .Interfaz = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "MostrarInterfaz", "")) = 1
        '.NoFrecuentes = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "NoFrecuentes", "")) = 1
        .ImprimirArchivo = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "ImprimirTraza", "")) = 1
        .ActualizarSucursalesOrigen = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "ActualizarSucursalesOrigen", "1")) = 1
        
        IniciarProceso = .BaseDatosAdm <> "" And .ServidorAdm <> "" And _
        .MontoPunto > 0
        
    End With
    
End Function

Public Function BuscarCorrelativo(pCn As ADODB.Connection, pCampo As String) As String

    Dim mRs As New ADODB.Recordset
    
    mRs.Open "Select * from ma_correlativos where cu_campo='" & pCampo & "' ", pCn, adOpenDynamic, adLockOptimistic, adCmdText
    If Not mRs.EOF Then
        mRs!nu_valor = mRs!nu_valor + 1
        BuscarCorrelativo = Format(mRs!nu_valor, "000000000#")
        mRs.Update
    End If
    
End Function

Public Function CrearCadenaConexion(pUser As String, pBaseDatos As String, pServidor As String, pPassword As String) As String
    CrearCadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" & pBaseDatos & ";Data Source=" & pServidor & ";" _
    & IIf(pUser = vbNullString Or pPassword = vbNullString, _
    "Persist Security Info=False;User ID=" & pUser & ";", _
    "Persist Security Info=True;User ID=" & pUser & ";Password=" & pPassword & ";")
End Function

Public Function CrearCadenaConexionShape(pUser As String, pBaseDatos As String, pServidor As String, pPassword As String) As String
    CrearCadenaConexionShape = "Provider=MSDataShape.1;Data Provider=SQLOLEDB.1;Connect Timeout=15;" _
    & "Initial Catalog=" & pBaseDatos & ";" & "Data Source=" & pServidor & ";" _
    & IIf(pUser = vbNullString Or pPassword = vbNullString, _
    "Persist Security Info=False;User ID=" & pUser & ";", _
    "Persist Security Info=True;User ID=" & pUser & ";Password=" & pPassword & ";")
End Function

Public Function CrearConexion(pCn, ByRef pUser As String, pBaseDatos As String, _
pServidor As String, ByRef pPassword As String, Optional AuthReset As Boolean = False, _
Optional ByRef NewUser As String, Optional ByRef NewPassword As String, _
Optional pShape As Boolean = False) As Boolean
    
Retry:
    
    On Error GoTo ErrHandler
    
    If pCn.State = adStateOpen Then pCn.Close
    
    pCn.CursorLocation = adUseServer
    
    If pShape Then
        pCn.Open CrearCadenaConexionShape(pUser, pBaseDatos, pServidor, pPassword)
    Else
        pCn.Open CrearCadenaConexion(pUser, pBaseDatos, pServidor, pPassword)
    End If
    
    pCn.CommandTimeout = 0
    
    CrearConexion = True
    
    Exit Function
    
ErrHandler:
    
    mErrDesc = Err.Description
    mErrNumber = Err.Number
    mErrSrc = Err.Source
    
    If AuthReset Then
        
        If Err.Number = -2147217843 Then
            
            Dim mClsTmp As Object
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If mClsTmp Is Nothing Then GoTo UnhandledErr
            
            TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
            
            If Not IsEmpty(TmpVar) Then
                pUser = TmpVar(0): pPassword = TmpVar(1)
                NewUser = TmpVar(2): NewPassword = TmpVar(3)
                Resume Retry
            End If
            
            Set mClsTmp = Nothing
            
        End If
        
    End If
    
UnhandledErr:
    
    GrabarErrores "CrearConexion", "", "", "", mErrDesc, mErrNumber
    
End Function

Public Sub GrabarErrores(ByVal pModulo As String, ByVal pCorrelativo, _
ByVal pTabla, ByVal pId, ByVal pError As String, Optional ByVal pNumError)
    
    Dim ErroresAgente As String
    
    ErroresAgente = App.Path & "\LogError.txt"
    
    Open ErroresAgente For Append Access Write As #1
    Print #1, Now & ", " & pModulo & "," & pCorrelativo & ", " & pTabla & "," _
    & vbNewLine & pId & ", " & pError & " (" & pNumError & ")"
    Close #1
    
End Sub

Public Function ExisteCampoTabla(pCampo As String, Optional pRs, Optional pSql As String = "", Optional pValidarFecha As Boolean = False, Optional pCn) As Boolean
    Dim mAux As Variant
    Dim mRs As New ADODB.Recordset
    On Error GoTo Error
    If Not IsMissing(pRs) Then
     
        mAux = pRs.Fields(pCampo).Value
        If pValidarFecha Then
            ExisteCampoTabla = mAux <> "01/01/1900"
        Else
            ExisteCampoTabla = True
        End If
    Else
        If IsMissing(pCn) Then
            ExisteCampoTabla = False
        Else
            mRs.Open pSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
        End If
        ExisteCampoTabla = True
        mRs.Close
    End If
    Exit Function
    
Error:
    'MsgBox Err.Description
    Err.Clear
    ExisteCampoTabla = False
End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function
