VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form Frm_ClienteFrecuente 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Clientes Frecuentes "
   ClientHeight    =   1845
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4080
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1845
   ScaleWidth      =   4080
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ProgressBar barra 
      Height          =   255
      Left            =   75
      TabIndex        =   6
      Top             =   1380
      Visible         =   0   'False
      Width           =   2025
      _ExtentX        =   3572
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.CommandButton cmd_salir 
      Caption         =   "&Salir"
      Height          =   645
      Left            =   3090
      TabIndex        =   5
      Top             =   1125
      Width           =   825
   End
   Begin VB.CommandButton cmd_execute 
      Caption         =   "&Ejecutar"
      Height          =   645
      Left            =   2235
      TabIndex        =   2
      Top             =   1125
      Width           =   825
   End
   Begin MSComCtl2.DTPicker desde 
      Height          =   330
      Left            =   120
      TabIndex        =   0
      Top             =   465
      Width           =   1650
      _ExtentX        =   2910
      _ExtentY        =   582
      _Version        =   393216
      Format          =   21233665
      CurrentDate     =   38642
   End
   Begin MSComCtl2.DTPicker hasta 
      Height          =   330
      Left            =   2325
      TabIndex        =   1
      Top             =   465
      Width           =   1650
      _ExtentX        =   2910
      _ExtentY        =   582
      _Version        =   393216
      Format          =   21233665
      CurrentDate     =   38642
   End
   Begin VB.Label Label2 
      Caption         =   "Hasta"
      ForeColor       =   &H8000000D&
      Height          =   180
      Left            =   2325
      TabIndex        =   4
      Top             =   225
      Width           =   555
   End
   Begin VB.Label Label1 
      Caption         =   "Desde"
      ForeColor       =   &H8000000D&
      Height          =   180
      Left            =   120
      TabIndex        =   3
      Top             =   225
      Width           =   555
   End
End
Attribute VB_Name = "Frm_ClienteFrecuente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmd_execute_Click()
    barra.Visible = True
    barra.Min = 0
     If Not Configuracion.NoFrecuentes Then
        Call mInterfaz.BajarVentasClientes(ConexOrigen, "", "", Configuracion.MontoPunto, Me.desde, Me.hasta, , Me.barra)
     Else
        Call mInterfaz.VentasClientesFrecNoFrec(ConexOrigen, "", "", Configuracion.MontoPunto, desde, hasta, Me.barra)
     End If
    barra.Visible = False
End Sub

Private Sub cmd_salir_Click()
    End
End Sub

Private Sub Form_Load()
    desde = Now
    hasta = Now
    
End Sub
