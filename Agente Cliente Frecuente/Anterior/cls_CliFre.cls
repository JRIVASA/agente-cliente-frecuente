VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_CliFre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mVarEventos                                         As String
Private mVarCorrelativo                                     As String



Property Let Evento(pValor As String)
    mVarEventos = pValor
End Property

Property Get Evento() As String
    Evento = mVarEventos
End Property

Property Let Correlativo(pValor As String)
    mVarCorrelativo = pValor
End Property

Property Get Correlativo() As String
    Correlativo = mVarCorrelativo
End Property

Public Function BajarVentasClientes(pCn As ADODB.Connection, pSeparador As String, pRutaArchivo As String, pMontoPunto As Double, Optional pFechaI As String = "", Optional pFechaF As String = "", Optional pCliente As String = "", Optional pBarra As Object = Nothing) As Boolean
    Dim mCnAdm As New ADODB.Connection, mRs As New ADODB.Recordset
    Dim mRsAdm As New ADODB.Recordset, mRsTrAdm As New ADODB.Recordset, mRsSorteos As New ADODB.Recordset
    Dim mConsulta As String, mWhere As String
    Dim mPuntos As Long, MTotal As Double, mLineaItem As Integer
    Dim mDenominacion As String, mInicio As Boolean, mBeginAdm As Boolean
    Dim mFechaAux As String
    
    On Error GoTo Err_Manager
    mIDFila = 0
    
   
    If pFechaI <> "" Then
        mFechaAux = FormatDateTime(pFechaI, vbShortDate) & " 00:00:00"
        mWhere = " and ma_pagos.f_fecha >='" & mFechaAux & "' "
    End If
    If pFechaF <> "" Then
        mFechaAux = FormatDateTime(pFechaF, vbShortDate) & " 23:59:59"
        mWhere = mWhere & " and ma_pagos.f_fecha<='" & mFechaAux & "' "
    End If
    If pCliente <> "" Then
        mWhere = " and ma_pagos.c_cliente='" & pCliente & "' "
    End If
    
    pCn.BeginTrans
    mInicio = True
    Correlativo = BuscarCorrelativo(pCn, "cs_numero_bajada")
    mConsulta = "Update ma_pagos set cs_numero_bajada='" & Correlativo & "' where cs_numero_bajada='' and c_cliente<>'9999999999' " & mWhere
    pCn.Execute mConsulta
    mConsulta = "SELECT MA_PAGOS.C_Cliente,ma_pagos.c_numero as Facturas,MA_PAGOS.F_Fecha as Fecha, MA_PAGOS.N_Total/" & pMontoPunto & " as Puntos, " _
             & " vad10.dbo.ma_clientes.c_descripcio as Cliente FROM MA_PAGOS INNER JOIN VAD10.dbo.MA_CLIENTES ON VAD10.dbo.MA_CLIENTES.c_CODCLIENTE = MA_PAGOS.C_Cliente " _
             & " AND VAD10.dbo.MA_CLIENTES.c_CODCLIENTE <> '9999999999' " _
             & " WHERE (MA_PAGOS.CS_NUMERO_BAJADA = '" & Correlativo & "') AND (MA_PAGOS.C_CONCEPTO = 'VEN') and ma_pagos.n_total>=" & pMontoPunto & mWhere
             '& " group by ma_pagos.c_cliente,vad10.dbo.ma_clientes.c_descripcio "
    
    mRs.CursorLocation = adUseClient
    mRs.Open mConsulta, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    mRs.ActiveConnection = Nothing
    If Not pBarra Is Nothing Then
        pBarra.Max = mRs.RecordCount + 1
        pBarra.Value = 1
    End If
    If Not mRs.EOF Then
        If CrearConexion(mCnAdm, CrearCadenaConexionShape(Configuracion.UsuarioAdm, Configuracion.BaseDatosAdm, Configuracion.ServidorAdm, Configuracion.PasswordAdm)) Then
            mCnAdm.BeginTrans
            mBeginAdm = True
            mConsulta = "Shape {Select * from ma_cambios_premios where 1=2} Append " _
                        & " ({Select * from tr_cambios_premios where 1=2} relate cs_documento to cs_documento,cs_tipo to cs_tipo) as Detalles"
            mRsAdm.Open mConsulta, mCnAdm, adOpenKeyset, adLockBatchOptimistic, adCmdText
            Do While Not mRs.EOF
                DoEvents
                mRsAdm.AddNew
                  Correlativo = BuscarCorrelativo(mCnAdm, "cs_clientefrecuente")
                  mRsAdm!cs_documento = Correlativo
                  mRsAdm!cs_tipo = "FAC"
                  mRsAdm!cs_codigocliente = mRs!c_cliente
                  mRsAdm!cs_Usuario = "9999999999"
                  mRsAdm!ds_fechacambio = Now
                  If Not pBarra Is Nothing Then
                     pBarra.Value = pBarra.Value + 1
                  End If
                  mRsAdm.Update
                  Set mRsTrAdm = mRsAdm("detalles").Value
                  mRsTrAdm.AddNew
                        mRsTrAdm!cs_documento = Correlativo
                        mRsTrAdm!cs_tipo = "FAC"
                        mRsTrAdm!cs_codigopremio = mRs!facturas
                        'If FormatDateTime(mRs!fechamin, vbShortDate) <> FormatDateTime(mRs!fechamax, vbShortDate) Then
                            'mRsTrAdm!cs_descripremio = "Entre el" & mRs!fechamin & " y " & mRs!fechamax
                        'Else
                            mRsTrAdm!cs_descripremio = "Fecha: " & mRs!fecha
                        'End If
                        mRsTrAdm!ns_valor = Fix(mRs!puntos)
                 mRsTrAdm.UpdateBatch
                 mCnAdm.Execute "update ma_clientes set nu_puntosacumulados=nu_puntosacumulados + " & Fix(mRs!puntos) & " where c_codcliente='" & mRs!c_cliente & "' "
                 mRs.MoveNext
            Loop
            mRsAdm.UpdateBatch
            mCnAdm.CommitTrans
            mBeginAdm = False
        End If
        pCn.CommitTrans
        mInicio = False
    End If
    BajarVentasClientes = True
    Exit Function

Err_Manager:
    If mInicio Then pCn.RollbackTrans
    If mBeginAdm Then mCnAdm.RollbackTrans
    MsgBox Err.Description, vbCritical
    
    
    
    
End Function

Public Function VentasClientesFrecNoFrec(pCn As ADODB.Connection, pSeparador As String, pRutaArchivo As String, pMontoPunto As Double, Optional pFechaI As String = "", Optional pFechaF As String = "", Optional pCliente As String = "", Optional pBarra As Object = Nothing) As Boolean
    Dim mCnAdm As New ADODB.Connection, mRs As New ADODB.Recordset
    Dim mRsAdm As New ADODB.Recordset, mRsTrAdm As New ADODB.Recordset, mRsSorteos As New ADODB.Recordset
    Dim mConsulta As String, mWhere As String
    Dim mPuntos As Long, MTotal As Double, mLineaItem As Integer
    Dim mDenominacion As String, mInicio As Boolean, mBeginAdm As Boolean
    Dim mFechaAux As String
    
    On Error GoTo Err_Manager
    mIDFila = 0
    
   
    If pFechaI <> "" Then
        mFechaAux = FormatDateTime(pFechaI, vbShortDate) & " 00:00:00"
        mWhere = " and ma_pagos.f_fecha >='" & mFechaAux & "' "
    End If
    If pFechaF <> "" Then
        mFechaAux = FormatDateTime(pFechaF, vbShortDate) & " 23:59:59"
        mWhere = mWhere & " and ma_pagos.f_fecha<='" & mFechaAux & "' "
    End If
    If pCliente <> "" Then
        mWhere = " and ma_pagos.c_cliente='" & pCliente & "' "
    End If
    
    mConsulta = "SELECT MA_PAGOS.C_cliente,min(ma_pagos.c_numero)+ ' - ' + max(ma_pagos.c_numero) as Facturas,min(MA_PAGOS.F_Fecha) as FechaMin, max(ma_pagos.f_fecha) as FechaMax, sum(MA_PAGOS.N_Total)/" & pMontoPunto & " as Puntos, " _
             & " vad10.dbo.ma_clientes.c_codcliente as Cliente FROM MA_PAGOS INNER JOIN VAD10.dbo.MA_CLIENTES ON VAD10.dbo.MA_CLIENTES.c_rif = MA_PAGOS.C_rif " _
             & " WHERE (MA_PAGOS.CS_NUMERO_BAJADA = '') AND (MA_PAGOS.C_CONCEPTO = 'VEN') and ma_pagos.c_cliente='9999999999' and ma_pagos.n_total>=" & pMontoPunto & mWhere _
             & " group by ma_pagos.c_cliente,vad10.dbo.ma_clientes.c_codcliente "
    
    mRs.CursorLocation = adUseClient
    mRs.Open mConsulta, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    mRs.ActiveConnection = Nothing
    If Not pBarra Is Nothing Then
        pBarra.Max = mRs.RecordCount
        pBarra.Value = 1
    End If
    If Not mRs.EOF Then
        If CrearConexion(mCnAdm, CrearCadenaConexionShape(Configuracion.UsuarioAdm, Configuracion.BaseDatosAdm, Configuracion.ServidorAdm, Configuracion.PasswordAdm)) Then
            mCnAdm.BeginTrans
            mBeginAdm = True
            mConsulta = "Shape {Select * from ma_cambios_premios where 1=2} Append " _
                        & " ({Select * from tr_cambios_premios where 1=2} relate cs_documento to cs_documento,cs_tipo to cs_tipo) as Detalles"
            mRsAdm.Open mConsulta, mCnAdm, adOpenKeyset, adLockBatchOptimistic, adCmdText
            Do While Not mRs.EOF
                mRsAdm.AddNew
                  Correlativo = BuscarCorrelativo(mCnAdm, "cs_clientefrecuente")
                  mRsAdm!cs_documento = Correlativo
                  mRsAdm!cs_tipo = "FAC"
                  mRsAdm!cs_codigocliente = mRs!Cliente
                  mRsAdm!cs_Usuario = "9999999999"
                  mRsAdm!ds_fechacambio = Now
                  If Not pBarra Is Nothing Then
                     pBarra.Value = pBarra.Value + 1
                  End If
                  mRsAdm.Update
                  Set mRsTrAdm = mRsAdm("detalles").Value
                  mRsTrAdm.AddNew
                        mRsTrAdm!cs_documento = Correlativo
                        mRsTrAdm!cs_tipo = "FAC"
                        mRsTrAdm!cs_codigopremio = mRs!facturas
                        If FormatDateTime(mRs!fechamin, vbShortDate) <> FormatDateTime(mRs!fechamax, vbShortDate) Then
                            mRsTrAdm!cs_descripremio = "Entre el" & mRs!fechamin & " y " & mRs!fechamax
                        Else
                            mRsTrAdm!cs_descripremio = "Del " & mRs!fechamin
                        End If
                        mRsTrAdm!ns_valor = Fix(mRs!puntos)
                 mRsTrAdm.UpdateBatch
                 mCnAdm.Execute "update ma_clientes set nu_puntosacumulados=nu_puntosacumulados + " & Fix(mRs!puntos) & " where c_codcliente='" & mRs!Cliente & "' "
                 mRs.MoveNext
            Loop
            mRsAdm.UpdateBatch
            mCnAdm.CommitTrans
            mBeginAdm = False
        End If
        
    End If
    VentasClientesFrecNoFrec = True
    Exit Function

Err_Manager:
    
    If mBeginAdm Then mCnAdm.RollbackTrans
    MsgBox Err.Description, vbCritical
    
    
    
    
End Function

