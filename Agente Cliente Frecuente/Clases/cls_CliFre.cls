VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_CliFre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mVarEventos                                         As String
Private mVarCorrelativo                                     As String
Private mvarUsuarioSupervisor                               As String

Property Let Evento(pValor As String)
    mVarEventos = pValor
End Property

Property Get Evento() As String
    Evento = mVarEventos
End Property

Property Let Correlativo(pValor As String)
    mVarCorrelativo = pValor
End Property

Property Get Correlativo() As String
    Correlativo = mVarCorrelativo
End Property

Property Let UsuarioSupervisor(pValor As String)
    mvarUsuarioSupervisor = pValor
End Property

Property Get UsuarioSupervisor() As String
    UsuarioSupervisor = mvarUsuarioSupervisor
End Property

Public Function ActualizarClientesFrecuentes(pCn As ADODB.Connection) As Boolean
    
    Dim mRs As New ADODB.Recordset
    Dim mCn As New ADODB.Connection
    
    Dim mSucursal As String
    
    On Error GoTo Errores
    
    mSucursal = BuscarSucursalOrigen(pCn)
    UsuarioSupervisor = BuscarUsuarioSupervisor(pCn)
    
    If mSucursal <> Empty Then
        
        If ActualizarPuntosCambiosPremios(pCn, mSucursal) Then
            
            If Configuracion.ActualizarSucursalesOrigen Then
                
                mRs.Open "Select * from ma_sucursales where c_estado=1 and B_TRASMITIR = 1", _
                pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                Dim ContieneCamposLogin As Boolean, TempUser As String, TempPassword As String
                ContieneCamposLogin = ExisteCampoTabla("cs_User", mRs, , , pCn) And ExisteCampoTabla("cs_Password", mRs, , , pCn)
                
                Do While Not mRs.EOF
                    
                    If ContieneCamposLogin Then
                        TempUser = mRs!cs_User: TempPassword = mRs!cs_Password
                    Else
                        TempUser = Configuracion.UsuarioAdm: TempPassword = Configuracion.PasswordAdm
                    End If
                    
                    If CrearConexion(mCn, TempUser, Configuracion.BaseDatosAdm, mRs!c_servidor, TempPassword) Then
                        Call ActualizarSucursales(pCn, mRs!c_Codigo, mCn)
                    End If
                    
                    mRs.MoveNext
                    
                Loop
                
            End If
            
            EliminarCambiosPremiosConsolidados pCn
            
        End If
        
    End If
    
    Exit Function
    
Errores:
    
    GrabarErrores "Actualizando Cliente Frecuente (Inicio)", "", "", "", Err.Description, Err.Number
    
    Err.Clear
    
End Function

Private Function ActualizarPuntosCambiosPremios(pCn, pSucursal) As Boolean
    
    Dim mRs As New ADODB.Recordset, mRsCambios As New ADODB.Recordset
    Dim mRsDet As ADODB.Recordset, mTransaccion As Boolean
    Dim mSql As String, mSqlDet As String, mMaxCorrelativo As String
    Dim nCampo As Integer
    
    On Error GoTo Errores
    
    mMaxCorrelativo = BuscarUltimoCorrelativoSucursal(pCn, pSucursal)
    
    mRs.CursorLocation = adUseClient
    
    mRs.Open "SELECT * FROM MA_CAMBIOS_PREMIOS_CONSOLIDADOS " & _
    IIf(mMaxCorrelativo <> "", "WHERE cs_Numero_Bajada > '" & mMaxCorrelativo & "' ", ""), _
    pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    mRs.ActiveConnection = Nothing
    
    If Not mRs.EOF Then
        
        pCn.BeginTrans: mTransaccion = True
        
        mSql = "Select * from MA_CAMBIOS_PREMIOS where 1=2 "
        mSqlDet = "Select * from TR_CAMBIOS_PREMIOS where 1=2 "
        mSql = "Shape {" & mSql & "} Append ({" & mSqlDet & "} relate cs_documento to cs_documento, " & _
        "cs_tipo to cs_tipo, cs_sucursal to cs_sucursal) as Detalles "
        mRsCambios.Open mSql, pCn, adOpenKeyset, adLockBatchOptimistic, adCmdText
        
        Do While Not mRs.EOF
            
            mRsCambios.AddNew
                
                mRsCambios!cs_Documento = mRs!cs_Documento
                mRsCambios!ds_FechaCambio = mRs!ds_FechaCambio
                mRsCambios!cs_CodigoCliente = mRs!cs_CodigoCliente
                mRsCambios!cs_Tipo = IIf(UCase(mRs!cs_Tipo) = "VEN", "FAC", "DEV")
                mRsCambios!cs_Sucursal = mRs!cs_Sucursal
                mRsCambios!cs_Usuario = UsuarioSupervisor
                
                Set mRsDet = mRsCambios("Detalles").Value
                
                mRsDet.AddNew
                    mRsDet!cs_Documento = mRs!cs_Documento
                    mRsDet!cs_Tipo = IIf(UCase(mRs!cs_Tipo) = "VEN", "FAC", "DEV")
                    mRsDet!cs_CodigoPremio = "F" & mRs!cs_Documento
                    mRsDet!cs_DescriPremio = "Factura de " & IIf(UCase(mRs!cs_Tipo) = "VEN", "Compra", "Devolución por Compra")
                    mRsDet!ns_Valor = Abs(Fix(mRs!nu_Puntos))
                    mRsDet!ns_Cantidad = 1
                    mRsDet!nu_TipoPremio = 0
                mRsDet.UpdateBatch
                
            mRsCambios.Update
            
            If Not ActualizarPuntosSucursalOrigen(pCn, mRs!cs_CodigoCliente, mRs!nu_Puntos) Then
                GoTo Errores
            End If
            
            If Configuracion.ImprimirArchivo Then ImprimirDebug "Reg " & mRs.AbsolutePosition & " de " & mRs.RecordCount
            
            mRs.MoveNext
            
        Loop
        
        mRsCambios.UpdateBatch
        
        Call GrabarCorridaAgente(pCn, pSucursal)
        
        pCn.CommitTrans: mTransaccion = False
        
        If Configuracion.ImprimirArchivo Then ImprimirDebug "", True
        
        ActualizarPuntosCambiosPremios = True
        
    End If
    
    If mTransaccion Then pCn.RollbackTrans
    
    Exit Function
    
Errores:
    
    If mTransaccion Then pCn.RollbackTrans
    
    'MsgBox Err.Description
    
    GrabarErrores "Grabando Cambios Premios", "", "", "", Err.Description, Err.Number
    
    'pCn.RollbackTrans
    
End Function

Private Function ActualizarSucursales(pCnOrigen, pSucursal As String, Optional pCnDestino) As Boolean
    
    Dim mRs As New ADODB.Recordset
    Dim mSql As String, mMaxCorrelativo As String, mTransaccion As Boolean
    Dim mInicio As Boolean
    
    On Error GoTo Errores
    
    mMaxCorrelativo = BuscarUltimoCorrelativoSucursal(pCnOrigen, pSucursal)
    
    mSql = "SELECT cs_CodigoCliente, ROUND(nu_Puntos, 0, 1) AS nu_Puntos FROM MA_CAMBIOS_PREMIOS_CONSOLIDADOS " & _
    IIf(mMaxCorrelativo <> "", " WHERE cs_Numero_Bajada > '" & mMaxCorrelativo & "' ", Empty)
    
    mRs.Open mSql, pCnOrigen, adOpenDynamic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
        
        If IsMissing(pCnDestino) Then
            If ActualizarPuntosSucursal(pCnOrigen, mRs) Then
                ActualizarSucursales = GrabarCorridaAgente(pCnOrigen, pSucursal)
            End If
        Else
            pCnDestino.BeginTrans: mInicio = True
            If ActualizarPuntosSucursal(pCnDestino, mRs) Then
                If GrabarCorridaAgente(pCnOrigen, pSucursal) Then
                    pCnDestino.CommitTrans: mInicio = False
                    ActualizarSucursales = True
                End If
            End If
        End If
        
    Else
        ActualizarSucursales = True
    End If
    
    Exit Function
    
Errores:
    
    If mInicio Then
        pCnDestino.RollbackTrans
    End If
    
    GrabarErrores "Actualizando Cliente Sucursales", "", "", "", Err.Description, Err.Number
    
End Function

Private Function ActualizarPuntosSucursal(pCn, pRs As ADODB.Recordset) As Boolean
    
    Dim mReg
    On Error GoTo Errores
    
    Do While Not pRs.EOF
        
        pCn.Execute "UPDATE MA_CLIENTES SET " & _
        "nu_PuntosAcumulados = nu_PuntosAcumulados + (" & Fix(pRs!nu_Puntos) & ") " & _
        "WHERE c_CodCliente = '" & pRs!cs_CodigoCliente & "' ", mReg
        
        pRs.MoveNext
        
    Loop
    
    ActualizarPuntosSucursal = True
    
    Exit Function
    
Errores:
    
    GrabarErrores "Grabando Puntos Clientes", "", "", "", Err.Description, Err.Number
    
End Function

Private Function ActualizarPuntosSucursalOrigen(pCn, pCodigoCliente As String, pPuntosFactura) As Boolean
    
    Dim mReg
    On Error GoTo Errores
    
    pCn.Execute "UPDATE MA_CLIENTES SET " & _
    "nu_PuntosAcumulados = nu_PuntosAcumulados + (" & Fix(pPuntosFactura) & ") " & _
    "WHERE c_CodCliente = '" & pCodigoCliente & "' ", mReg
    
    ActualizarPuntosSucursalOrigen = True
    
    Exit Function
    
Errores:
    
    GrabarErrores "Grabando Puntos Clientes", "", "", "", Err.Description, Err.Number
    
End Function

Private Function BuscarSucursalOrigen(pCn) As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSqlWhere As String
    
    mSqlWhere = IIf(Trim(Configuracion.LocalidadOrigen) <> "", " AND c_Codigo = '" & Configuracion.LocalidadOrigen & "' ", "")
    
    mRs.Open "SELECT c_Codigo FROM MA_SUCURSALES WHERE b_Trasmitir = 0 " & mSqlWhere, _
    pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarSucursalOrigen = mRs!c_Codigo
    End If
    
    mRs.Close
    
End Function

Private Function BuscarUltimoCorrelativoSucursal(pCn, pSucursal) As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSql As String
    
    mSql = "SELECT MAX(cs_Documento) AS MaxDoc FROM MA_CORRIDAS_AGENTE_CLIENTES WHERE cs_Sucursal = '" & pSucursal & "' "
    mRs.Open mSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarUltimoCorrelativoSucursal = IIf(IsNull(mRs!MaxDoc), "", mRs!MaxDoc)
    End If
    
    mRs.Close
    
End Function

Private Function GrabarCorridaAgente(pCn, pSucursal) As Boolean
    
    Dim mRs As New ADODB.Recordset
    Dim mCorrelativo As String, mSql As String
    
    On Error GoTo Errores
    
    mCorrelativo = BuscarUltimoCorrelativo(pCn)
    
    mRs.Open "SELECT * FROM MA_CORRIDAS_AGENTE_CLIENTES WHERE 1 = 2", _
    pCn, adOpenDynamic, adLockOptimistic, adCmdText
    
    mRs.AddNew
        mRs!cs_Documento = mCorrelativo
        mRs!cs_Sucursal = pSucursal
        mRs!ds_Fecha = FechaBD(Now, FBD_FULL, True)
    mRs.Update
    
    mRs.Close
    
    GrabarCorridaAgente = True
    
    Exit Function
    
Errores:
    
    GrabarErrores "Grabando Corrida Agente", "", "", "", Err.Description, Err.Number
    Err.Clear
    
End Function

Private Function BuscarUltimoCorrelativo(pCn, Optional esMinimo As Boolean = False) As String
    
    Dim mRs As New ADODB.Recordset
    
    mRs.Open "SELECT " & IIf(esMinimo, "MIN", "MAX") & " (cs_Numero_Bajada) AS Ultimo FROM MA_CAMBIOS_PREMIOS_CONSOLIDADOS ", _
    pCn, adOpenDynamic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarUltimoCorrelativo = mRs!Ultimo
    End If
    
End Function

Private Sub EliminarCambiosPremiosConsolidados(pCn As ADODB.Connection)
    
    Dim mSql As String
    
    mSql = "DELETE FROM MA_CAMBIOS_PREMIOS_CONSOLIDADOS WHERE cs_Numero_Bajada <= '" & BuscarUltimoCorrelativoClientes(pCn) & "' "
    
    'MsgBox pCn.ConnectionString
    
    pCn.Execute mSql, Reg
    
End Sub

Private Function BuscarUltimoCorrelativoClientes(pCn) As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSql As String
    
    mSql = "SELECT MAX(cs_Documento) AS Doc FROM MA_CORRIDAS_AGENTE_CLIENTES "
    
    mRs.CursorLocation = adUseClient
    
    mRs.Open mSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    mRs.ActiveConnection = Nothing
    
    If Not mRs.EOF Then
        BuscarUltimoCorrelativoClientes = mRs!Doc
    End If
    
End Function

Private Function BuscarUsuarioSupervisor(pCn) As String
    
    Dim mRs As New ADODB.Recordset
    
    On Error GoTo Errores
    
    mRs.Open "SELECT * FROM VAD10.DBO.MA_USUARIOS WHERE CodUsuario LIKE '999999%'", _
    pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarUsuarioSupervisor = mRs!CodUsuario
    Else
        GoTo Errores
    End If
    
    mRs.Close
    
    Exit Function
    
Errores:
    
    Err.Clear
    BuscarUsuarioSupervisor = "9999999999"
    
End Function

Private Sub ImprimirDebug(pCadena As String, Optional Cerrar As Boolean = False)
    
    Static AbrioCanal As Boolean
    Static mCanal As Integer
    
    If Not AbrioCanal Then
        AbrioCanal = True
        mCanal = FreeFile()
        Open App.Path & "\ClientesDebug.txt" For Append Access Write As #mCanal
    End If
    
    If Not Cerrar Then
        Print #mCanal, Now & ", " & pCadena
    Else
        Close #mCanal
    End If
          
End Sub
